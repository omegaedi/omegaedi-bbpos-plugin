'use strict';

var BBPOS = ( typeof BBPOS === 'undefined' ? {} : BBPOS );
var cordova = window.cordova || window.Cordova,
    fail = function(error) {
        console.log('Error running your request: ' + error); //TODO: Rollbar this boy like a doobie
    };

BBPOS.startTransaction = function(callback, error, arg0) {
    var success = function(postBody) {
        callback(postBody);
    };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'startTransaction', [arg0]);
};


BBPOS.isDeviceConnected = function(callback, error) {
    var success = function(connected) {
        callback(connected);
    };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'isDeviceConnected', []);
};

BBPOS.connectDevice = function(callback, error, arg0) {
    console.log("About to connect to card reader");
    var success = function(choice) {
        callback(choice);
    };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'connectDevice', [arg0]);
};

BBPOS.defaultDevice = function(callback, error) {
    var success = function(choice) {
        callback(choice);
    };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'defaultDevice', []);
};

BBPOS.isDeviceOpened = function(callback, error) {
    var success = function(opened) { callback(opened); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'isDeviceOpened', []);
};

BBPOS.openDevice = function(callback, error) {
    var success = function(status) { callback(status); }
    var fail_handler = error || fail;;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'openDevice', []);
};
BBPOS.closeDevice = function(callback, error) {
    var success = function(status) { callback(status); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'closeDevice', []);
};
BBPOS.disconnectDevice = function(callback, error) {
    var success = function(status) { callback(status); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'disconnectDevice', []);
};
BBPOS.clearCardData = function(callback, error) {
    var success = function(data_cleared) { callback(data_cleared); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'clearCardData', []);
};
BBPOS.getBatteryLevel = function(callback, error) {
    var success = function(battery) { callback(battery); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getBatteryLevel', []);
};
BBPOS.setCardData = function(callback, error) {
    var success = function(data_set) { callback(data_set); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'setCardData', []);
};
BBPOS.getTrackDecodeStatus = function(callback, error) {
    var success = function(decode_status) { callback(decode_status); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getTrackDecodeStatus', []);
};
BBPOS.getTrack1 = function(callback, error) {
    var success = function(track) { callback(track); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getTrack1', []);
};
BBPOS.getTrack2 = function(callback, error) {
    var success = function(track) { callback(track); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getTrack2', []);
};
BBPOS.getTrack3 = function(callback, error) {
    var success = function(track) { callback(track); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getTrack3', []);
};
BBPOS.getTrack1Masked = function(callback, error) {
    var success = function(track) { callback(track); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getTrack1Masked', []);
};
BBPOS.getTrack2Masked = function(callback, error) {
    var success = function(track) { callback(track); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getTrack2Masked', []);
};
BBPOS.getTrack3Masked = function(callback, error) {
    var success = function(track) { callback(track); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getTrack3Masked', []);
};
BBPOS.getMagnePrintStatus = function(callback, error) {
    var success = function(status) { callback(status); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getMagnePrintStatus', []);
};
BBPOS.getMagnePrint = function(callback, error) {
    var success = function(magne_print) { callback(magne_print); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getMagnePrint', []);
};
BBPOS.getDeviceSerial = function(callback, error) {
    var success = function(device_serial) { callback(device_serial); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getDeviceSerial', []);
};
BBPOS.getSessionID = function(callback, error) {
    var success = function(session_id) { callback(session_id); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getSessionID', []);
};


//..... A few methods skipped

BBPOS.getDevices = function(callback, error) {
    var success = function(devices) { callback(devices); };
    var fail_handler = error || fail;
    console.log('Made it to BBPOS.js, about to call cordova.exec');

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getDevices', []);
};

BBPOS.getPaired = function(callback, error) {
    var success = function(paired) { callback(paired); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getPaired', []);
};

BBPOS.getSavedDevice = function(callback, error) {
    var success = function(device) { callback(device); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getSavedDevice', []);
}

BBPOS.setSavedDevice = function(callback, error, arg0){
    var success = function(address) { callback(address);};
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'setSavedDevice', [arg0])
}

BBPOS.setAmount = function(callback, error, arg0){
    var success = function(status) { callback(status); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'setAmount', [arg0])
}

BBPOS.setDeviceProtocolString = function(callback, protocol_string) {
    var success = function(status) { callback(status); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'setDeviceProtocolString', [protocol_string]);
};
BBPOS.listenForEvents = function(callback, error) {
    var success = function(status) { callback(status); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'listenForEvents', []);
};
BBPOS.listenForCardEvents = function(callback, error) {
    var success = function(status) { callback(status); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'listenForCardEvents', []);
};

BBPOS.getCardName = function(callback, error) {
    var success = function(card_name) { callback(card_name); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getCardName', []);
};
BBPOS.getCardIIN = function(callback, error) {
    var success = function(card_iin) { callback(card_iin); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getCardIIN', []);
};
BBPOS.getCardLast4 = function(callback, error) {
    var success = function(card_last4) { callback(card_last4); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getCardLast4', []);
};
BBPOS.getCardExpDate = function(callback, error) {
    var success = function(card_exp_date) { callback(card_exp_date); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getCardExpDate', []);
};
BBPOS.getCardServiceCode = function(callback, error) {
    var success = function(card_svc) { callback(card_svc); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getCardServiceCode', []);
};
BBPOS.getCardStatus = function(callback, error) {
    var success = function(card_status) { callback(card_status); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getCardStatus', []);
};


//..... A few methods skipped


BBPOS.getAddress = function(callback, error) {
    var success = function(dev_address) { callback(dev_address); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'getDevAddress', []);
};

BBPOS.setDeviceType = function(callback, error) {
    var success = function(status) { callback(status); };
    var fail_handler = error || fail;

    cordova.exec(success, fail_handler, 'com.omegaedi.bbpos', 'setDeviceType', []);
};


module.exports = BBPOS;
