package com.omegaedi.cordova.plugins.bbpos;

import java.math.BigDecimal;
import java.util.*;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CallbackContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.util.Log;
import android.Manifest;
import android.widget.Toast;

import net.authorize.Environment;
import net.authorize.Merchant;
import net.authorize.aim.emv.EMVDeviceConnectionType;
import net.authorize.aim.emv.EMVErrorCode;
import net.authorize.aim.emv.EMVTransaction;
import net.authorize.aim.emv.EMVTransactionManager;
import net.authorize.aim.emv.EMVTransactionType;
import net.authorize.aim.emv.EmvSdkUISettings;
import net.authorize.auth.PasswordAuthentication;

public class BBPOS extends CordovaPlugin {
    private static final String TAG = "BBPOSPlugin";
    public static final int REQ_CODE = 0;

    CallbackContext callbackContext;
    Context context;
    JSONArray executeArgs;

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        Log.d(TAG, "Initializing BBPOS Plugin");
        this.context = cordova.getActivity().getApplicationContext();
        EmvSdkUISettings.setToastColor(Color.GREEN);
    }

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        this.callbackContext = callbackContext;
        this.executeArgs = args;
        checkRuntimePermissions();
        this.context = cordova.getActivity().getApplicationContext();

        switch (action) {
            case "getDevices":
                getDevices();
                return true;
            case "connectDevice":
                connectDevice();
                return true;
            case "disconnectDevice":
                disconnectDevice();
                return true;
            case "isDeviceConnected":
                isDeviceConnected();
                return true;
            case "getSavedDevice":
                getSavedDevice();
                return true;
            case "setSavedDevice":
                setSavedDevice();
                return true;
            case "startTransaction":
                startTransaction();
                return true;
        }
        return false;
    }

    private void connectDevice() throws JSONException {
        Log.d("connectDevice", "Connecting to reader");
        BluetoothDevice btDevice = null;

        if (executeArgs.length() != 0 && executeArgs.get(0) != JSONObject.NULL && !executeArgs.get(0).toString().equals("")){
            btDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(executeArgs.get(0).toString());
        }

        this.setSavedDevice();

        EMVTransactionManager.connectBTDevice(this.context, btDevice, emvTransactionListener);
    }

    private void disconnectDevice() {
        Log.d("disconnectDevice", "Disconnecting from reader");
        EMVTransactionManager.disconnectBTDevice(this.context, emvTransactionListener);
    }

    public void getDevices() {
        toast("Scanning for Bluetooth devices");
        Log.d("getDevices", "Starting scan...");
        EMVTransactionManager.setDeviceConnectionType(EMVDeviceConnectionType.BLUETOOTH);
        EMVTransactionManager.startBTScan(this.context, emvTransactionListener);
    }

    private void isDeviceConnected() {
        EMVTransactionManager.EMVReaderStatus status = EMVTransactionManager.getEmvReaderStatus();
        Log.d("isDeviceConnected", status.toString());

        boolean isConnected = !status.equals(EMVTransactionManager.EMVReaderStatus.CONNECTION_NOT_STARTED);
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, isConnected));
    }

    private void getSavedDevice() {
        SharedPreferences settings = context.getSharedPreferences("com.omegaedi.bbpos.MyPrefsFile", 0);
        String saved = settings.getString("ReaderAddress", "");

        if(saved.equals("")){
            saved = "No Address";
        }

        Log.d("getSavedDevice", "Address: "+saved);
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, saved));
    }

    private void setSavedDevice() throws JSONException {
        String deviceAddress = executeArgs.getString(0);
        SharedPreferences settings = context.getSharedPreferences("com.omegaedi.bbpos.MyPrefsFile", 0);
        Log.i(TAG, "Trying to set them preferences to save a device");
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("ReaderAddress", deviceAddress);
        editor.commit();
    }

    private void startTransaction() throws JSONException {
        PasswordAuthentication passAuth = PasswordAuthentication
                .createMerchantAuthentication("Dummy Login", "Dummy Password", "Dummy Thicc");
        Merchant merchant = Merchant.createMerchant(Environment.PRODUCTION, passAuth);

        BigDecimal amount = new BigDecimal(executeArgs.getString(0));

        Log.d("startTransaction", "Starting transaction for amount: "+amount);

        EMVTransaction transaction = EMVTransactionManager.createEMVTransaction(merchant, amount);
        transaction.setEmvTransactionType(EMVTransactionType.PAYMENT);
        EMVTransactionManager.setDeviceConnectionType(EMVDeviceConnectionType.BLUETOOTH);

        EMVTransactionManager.startQuickChipTransaction(transaction, emvTransactionListener, cordova.getActivity());
    }

    void toast(String str) {
        Toast.makeText(
                webView.getContext(),
                str,
                Toast.LENGTH_LONG
        ).show();
    }

    private void addPermission(List<String> permissionsList, String permission) {
//        Log.d("addPermissions", permission + ": " + cordova.hasPermission(permission));
        if (!cordova.hasPermission(permission)) {
            Log.d("addPermissions", "Don't have "+permission);
            permissionsList.add(permission);
        }
    }

    private void checkRuntimePermissions() {
        final List<String> permissionsList = new ArrayList<>();

        addPermission(permissionsList, Manifest.permission.RECORD_AUDIO);
        addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE);
        addPermission(permissionsList, Manifest.permission.BLUETOOTH);
        addPermission(permissionsList, Manifest.permission.BLUETOOTH_ADMIN);
        addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION);
        addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION);

        if (!permissionsList.isEmpty()) {
            String[] permissionsArray = permissionsList.toArray(new String[0]);

            cordova.requestPermissions(this, REQ_CODE, permissionsArray);
        }
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
        for (int r : grantResults) {
            if (r == PackageManager.PERMISSION_DENIED) {
                this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "Permission denied."));
                return;
            }
        }
    }

    EMVTransactionManager.QuickChipTransactionSessionListener emvTransactionListener = new EMVTransactionManager.QuickChipTransactionSessionListener() {
        @Override
        public void onReturnBluetoothDevices(final List<BluetoothDevice> bluetoothDeviceList) {
            Log.d("onReturnBTDevices", "Returning Bluetooth devices");
            toast("Got devices!");
            JSONArray devicesJSON = new JSONArray();
            for (BluetoothDevice device : bluetoothDeviceList) {
                JSONObject deviceJSON = new JSONObject();
                try {
                    deviceJSON.put("label", device.getName());
                    deviceJSON.put("value", device.getAddress());
                    devicesJSON.put(deviceJSON);
                } catch (JSONException err) {
                    Log.d("onReturnBTDevices", "Problem with the JSON, yo");
                }
            }

            PluginResult pr = new PluginResult(PluginResult.Status.OK, devicesJSON);
            callbackContext.sendPluginResult(pr);
        }

        @Override
        public void onBluetoothDeviceConnected(BluetoothDevice bluetoothDevice) {
            Log.d("Bluetooth device", "Bluetooth device connected : " + bluetoothDevice.getName());

            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, bluetoothDevice.getAddress()));
        }

        @Override
        public void onBluetoothDeviceDisConnected() {
            Log.d("Bluetooth device", "Bluetooth device disconnected");
            // Pretty sure this only gets called on an unexpected disconnect, not a deliberate one
        }

        @Override
        public void onTransactionStatusUpdate(String transactionStatus) {
            Log.d("Transaction status", transactionStatus);

            if (transactionStatus.equals("Disconnecting to bluetooth device")) {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, true));
            }
        }

        @Override
        public void onPrepareQuickChipDataSuccessful() {
            Log.d("Prepare Quick Chip Data", "Successful");
        }

        @Override
        public void onPrepareQuickChipDataError(EMVErrorCode error, String cause) {
            Log.d("onPrepareQCDataError", cause);
        }

        @Override
        public void onEMVTransactionSuccessful(net.authorize.aim.emv.Result result) {
            // processEmvTransactionResult(result, null);
            Log.d("EMVTransaction", "Successful");
        }

        @Override
        public void onEMVReadError(EMVErrorCode emvError) {
            Log.d("onEMVReadError", emvError.getErrorString());
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, emvError.getErrorString()));
        }

        @Override
        public void onEMVTransactionError(net.authorize.aim.emv.Result result, EMVErrorCode emvError) {
            String authnetPOSTbody = result.getRequestTransaction().toAuthNetPOSTString();
            Log.d("EMVTransaction", "Error: "+ emvError.toString());
            Log.d("AuthNet POST body", authnetPOSTbody);

            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, authnetPOSTbody));
        }

    };
}